package org.example.vending;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Danil Suits (danil@vast.com)
 */
class CalibrationTest {
    // The motivation for this enum is simply to get
    // nice error messages if the production component
    // doesn't return the answer that we want.
    enum Result {PASS, FAIL}

    @Test
    public void testCalibration() {
        assertEquals(
                Result.PASS,
                TestSubject.testResult(
                        Result.PASS,
                        Result.FAIL
                )
        );
    }
}
class TestSubject {
    static <T> T testResult(T pass, T fail) {
        return pass;
    }
}